#!/usr/bin/perl -w
use strict;
use Protocol::OSC;
use IO::Socket::INET;
use Getopt::Long;

my %h;
$h{DEBUG} = 0;
$h{OSCPort} = 57120;
$h{OSCPeer} = '1.3.1.2';
$h{volume} = 255;
GetOptions (\%h, 'OSCPort=i', 'OSCPeer=s', 'DEBUG', 'mac=s', 'volume=i');

if((!defined $h{mac})) {
    print STDERR "Usage: $0 --device=<wireless device>  <--OSCPeer=host> <--OSCPort=port> <--DEBUG> --volume=<volume> --mac=<mac>\n\n";
    exit 1;
}

my $macbin;
my (@hex) = $h{mac} =~ /([0-9a-f])/g;
for my $nibble (@hex) {
    my $dec = hex($nibble);
    my $nibbin = sprintf("%04b", $dec);
    $macbin .= $nibbin;
    print "($nibble, $dec) $nibbin\n";
}

    # position and amount of digits/bits
    my $n3 = substr($macbin, 0, 5);
    my $n2 = substr($macbin, 5, 5);
    my $n5 = substr($macbin, 10, 4);
    my $n4 = substr($macbin, 14, 4);
    my $n0 = substr($macbin, 18, 4);
    my $mode = substr($macbin, 22, 2);
    my $d5 = substr($macbin, 25, 3);
    my $d4 = substr($macbin, 28, 3);
    my $d3 = substr($macbin, 31, 3);
    my $d2 = substr($macbin, 34, 3);
    my $d1 = substr($macbin, 37, 3);
    my $d0 = substr($macbin, 40, 3);
    my $n1 = substr($macbin, 43, 5);
    my @values = map { oct('0b'.$_) } ($mode, $n0, $n1, $n2, $n3, $n4, $n5, $d0, $d1, $d2, $d3, $d4, $d5);
    push(@values, $h{volume});

print "Sending: ", join(' ', @values), "\n";

my $osc = Protocol::OSC->new;
my $data = $osc->message("/mac", "iiiiiiiiiiiii", @values);
my $udp = IO::Socket::INET->new( PeerAddr => $h{OSCPeer}, PeerPort => $h{OSCPort}, Proto => 'udp', Type => SOCK_DGRAM) || die $!;
$udp->send($data);


